# Web index

Description: [cralwer.md](CRAWLER.md)

## Prerequisites

Installed [sbt](https://www.scala-sbt.org/download.html)

## Running the application

```sbtshell
sbt "runMain com.plista.Server"
```

## Running the tests

Execute tests using:
```sbtshell
sbt test
```

## Build

Application could be build using [SBT Native Packager](https://www.scala-sbt.org/sbt-native-packager/)

For example:
```sbtshell
sbt universal:packageBin
```

To run application from build:

Extract zip
```
unzip target/universal/web-index-0.1.zip -d target/universal/
```

Run it
```
./target/universal/web-index-0.1/bin/web-index
```

## Submit url for crawling
Example:  

Request:  
```shell
curl -X POST \
  http://localhost:8080/crawl \
  -H 'Content-Type: application/json' \
  -d '{"urls": ["https://www.google.com/"]}'
```
Response status: _202 Accepted_  
Response body: ```{}```

## Search 
Request:  
```shell
curl -X GET 'http://localhost:8080/search?q=best'
```

Reponse:
```json
{
    "results": [
        {
            "url": "https://www.google.com/security/",
            "context": "together some quick tips and <em>best</em> practices for you to create"
        },
        {
            "url": "https://www.google.com/security/built-in-protection/",
            "context": "together some quick tips and <em>best</em> practices for you to create"
        }
    ]
}
```
  

