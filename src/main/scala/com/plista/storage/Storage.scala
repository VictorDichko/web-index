package com.plista.storage

import akka.actor.ActorSystem
import akka.event.Logging
import com.outr.lucene4s.DirectLucene
import com.outr.lucene4s.field.Field
import com.outr.lucene4s.query.Sort
import com.plista.Errors.{CustomError, CustomUnknownError}
import com.plista.Models.{FullSearchResults, SingleSearchResult}

import scala.util.control.NonFatal

class Storage(implicit system: ActorSystem) {

  import Storage._

  private val log = Logging(system, getClass)

  private val lucene = new DirectLucene(uniqueFields = List(UrlFieldName), defaultFullTextSearchable = true)

  private val urlField: Field[String] = lucene.create.field[String](UrlFieldName)
  private val textField: Field[String] = lucene.create.field[String](TextFieldName)

  def add(url: String, content: String): Either[CustomError, Unit] = {
    try {
      lucene.doc().fields(urlField(url), textField(content)).index()
      Right(())
    } catch {
      case NonFatal(e) =>
        val message = "Unable to add url to index: " + url
        log.error(e, message)
        Left(CustomUnknownError(message))
    }
  }

  def find(query: String): FullSearchResults = {
    val searchResults = lucene.query().sort(Sort.Score).filter(query).highlight().search()
    val extractedSearchResults = searchResults.results.map { result =>
      val highlightedContext = result.highlighting(textField).map { hlResult =>
        extractContext(hlResult.fragment)
      }.mkString(",")
      SingleSearchResult(url = result(urlField), context = highlightedContext)
    }
    FullSearchResults(results = extractedSearchResults)
  }

  private def extractContext(content: String, hlStart: String = "<em>", hlEnd: String = "<\\/em>"): String = {
    val regexp = """((?:\w+\W+){0,5}<em>.*<\/em>(?:\W+\w+){0,5})""".r
    regexp.findAllIn(content).mkString(";")
  }
}

object Storage {
  val UrlFieldName = "url"
  val TextFieldName = "text"
}
