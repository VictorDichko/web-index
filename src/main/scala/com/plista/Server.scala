package com.plista

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.plista.crawler.Crawler
import com.plista.search.SearchService
import com.plista.storage.Storage

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Server extends App {

  implicit val system: ActorSystem = ActorSystem("web-index")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  lazy val log = Logging(system, getClass)

  val storage = new Storage()
  val crawler = new Crawler(storage)
  crawler.startCrawling()
  val searchService = new SearchService(storage)
  val controller = new Controller(crawler, searchService)

  val routes: Route = controller.routes

  val serverBinding = Http().bindAndHandle(routes, "0.0.0.0", 8080)

  serverBinding.onComplete {
    case Success(bound) =>
      log.info(s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}/")
    case Failure(e) =>
      log.error(e, s"Server could not start!")
      system.terminate()
  }

  sys.addShutdownHook(shutdown())

  def shutdown(): Unit = {
    serverBinding
      .flatMap(_.unbind())
      .onComplete(_ ⇒ system.terminate())
  }

}


