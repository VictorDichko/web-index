package com.plista.crawler

import akka.stream.{Attributes, Outlet, SourceShape}
import akka.stream.stage.{GraphStage, GraphStageLogic, OutHandler}

import scala.collection.mutable


class UrlsReceiverSource(queue: mutable.Queue[String]) extends GraphStage[SourceShape[Option[String]]] {
  val out: Outlet[Option[String]] = Outlet("UrlsSourceOutput")
  override val shape: SourceShape[Option[String]] = SourceShape(out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {

    setHandler(out, new OutHandler {
      override def onPull(): Unit = {
        if (queue.nonEmpty) {
          val element = queue.dequeue()
          push(out, Some(element))
        } else {
          push(out, None)
        }
      }
    })
  }
}