package com.plista.crawler

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.model._
import akka.stream._
import akka.stream.scaladsl.{Flow, GraphDSL, MergePreferred, RunnableGraph, Sink, Source, Unzip}
import akka.{Done, NotUsed}
import com.plista.Errors.{CustomError, CustomUnknownError}
import com.plista.storage.Storage
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.elementList

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.Try
import scala.util.control.NonFatal

class Crawler(storage: Storage)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) {

  private val log = Logging(system, getClass)

  private val browser = JsoupBrowser()

  private val visitedLinks: mutable.Set[String] = mutable.Set.empty[String]

  private val urlsQueue: mutable.Queue[String] = mutable.Queue.empty[String]

  def addUrlsToCrawl(urls: Seq[String]): Either[CustomError, Unit] = {
    val urlsToAdd = urls.flatMap(uriOptFromString)
    if (urlsToAdd.isEmpty) {
      Left(CustomUnknownError("Unable to add urls for parsing", code = 400))
    } else {
      urlsQueue.enqueue(urls: _*)
      log.debug("Added urls to crawl: " + urls)
      Right(())
    }
  }

  private def uriOptFromString(link: String): Option[Uri] = {
    Try(Uri(link.trim).withoutFragment).recover {
      case NonFatal(e) =>
        log.error(e, s"Invalid url: $link, ")
        throw e
    }.toOption
  }

  private def toAbsolute(uri: Uri, authority: Uri.Authority, scheme: String): Uri = {
    if (uri.isRelative) {
      uri.withAuthority(authority).withScheme(scheme)
    } else uri
  }

  def urlToScraped: Flow[String, ((String, String), List[String]), NotUsed] = {
    Flow[String]
      .map { url => log.debug("urlToScraped start: " + url); url }
      .map(url => (url, browser.get(url)))
      .map { case (url, document) =>

        visitedLinks += url

        val foundLinks = document >> elementList("a") flatMap (_ >?> attr("href"))

        val currentPageUri = Uri(url)

        val normalizedLinks = foundLinks.distinct.flatMap { link =>
          uriOptFromString(link).map(uri => toAbsolute(uri, currentPageUri.authority, currentPageUri.scheme))
        }.distinct

        val linksToScrape = normalizedLinks.filter { link =>
          link.authority.host.address == currentPageUri.authority.host.address
        }.map(_.toString)

        ((url, document >> allText), linksToScrape)
      }.map { res => log.debug("urlToScraped end: " + res._2); res }
      .withAttributes(ActorAttributes.supervisionStrategy(decider))
  }

  val filterOutVisitedFlow: Flow[String, String, NotUsed] =
    Flow[String].filterNot(visitedLinks.contains)

  val flattenFlow: Flow[List[String], String, NotUsed] = {
    Flow[List[String]].mapConcat(identity).buffer(size = 10000, OverflowStrategy.dropHead)
  }

  val sink: Sink[(String, String), Future[Done]] = Sink.foreach[(String, String)] { case (link, content) =>
    log.debug(s"Saving page to storage with url: [$link] and content: ${content.take(100)}")
    storage.add(link, content)
  }

  def crawlerGraph: RunnableGraph[NotUsed] = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
    import GraphDSL.Implicits._
    val in = Source.fromGraph(new UrlsReceiverSource(urlsQueue))
    val tickSource = Source.tick(initialDelay = 0 seconds, 100 millis, tick = ())

    val tickUrlSource = tickSource.zip(in).collect { case ((), Some(url)) => url }
    val store = sink

    val unzip = builder.add(Unzip[(String, String), List[String]])
    val merge = builder.add(MergePreferred[String](1))

    tickUrlSource ~> merge ~> filterOutVisitedFlow ~> urlToScraped ~> unzip.in
    unzip.out0 ~> store
    unzip.out1 ~> flattenFlow ~> merge.preferred

    ClosedShape
  })


  private val decider: Supervision.Decider = {
    case _ => Supervision.Resume
  }

  def startCrawling(): Unit = {
    crawlerGraph.run()
  }

}
