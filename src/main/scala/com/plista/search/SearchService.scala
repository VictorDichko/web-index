package com.plista.search

import com.plista.Models.FullSearchResults
import com.plista.storage.Storage

class SearchService(storage: Storage) {

  def search(query: String): FullSearchResults = {
    storage.find(query)
  }

}
