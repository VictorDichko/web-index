package com.plista

object Errors {

  trait CustomError {
    def message: String
    def code: Int
  }

  case class CustomUnknownError(message: String, code: Int = 500) extends CustomError

}
