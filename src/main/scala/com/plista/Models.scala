package com.plista

object Models {

  case class CrawlParams(urls: Seq[String])

  case class SingleSearchResult(url: String, context: String)

  case class FullSearchResults(results: Seq[SingleSearchResult])

}
