package com.plista

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.plista.Errors.CustomUnknownError
import com.plista.Models.CrawlParams
import com.plista.crawler.Crawler
import com.plista.search.SearchService
import spray.json.JsObject

class Controller(crawler: Crawler, searchService: SearchService) extends JsonSupport {

  val crawlUrlsRoute: Route =
    path("crawl") {
      post {
        entity(as[CrawlParams]) { crawlParams =>
          crawler.addUrlsToCrawl(crawlParams.urls) match {
            case Right(_) =>
              complete(StatusCodes.Accepted, JsObject())
            case Left(error) =>
              val result = CustomUnknownError(error.message)
              complete(result.code, result)
          }
        }
      }
    }

  val searchRoute: Route =
    path("search") {
      get {
        parameters('q.as[String]) { query =>
          val result = searchService.search(query)
          complete(result)
        }

      }
    }

  val routes: Route = crawlUrlsRoute ~ searchRoute

}

