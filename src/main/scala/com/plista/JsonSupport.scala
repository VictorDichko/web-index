package com.plista

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.plista.Errors.CustomUnknownError
import com.plista.Models.{CrawlParams, FullSearchResults, SingleSearchResult}
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

trait JsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val CrawlUrlsFormats: RootJsonFormat[CrawlParams] = jsonFormat1(CrawlParams)
  implicit val CustomUnknownErrorFormats: RootJsonFormat[CustomUnknownError] = jsonFormat2(CustomUnknownError)
  implicit val SingleSearchResultFormats: RootJsonFormat[SingleSearchResult] = jsonFormat2(SingleSearchResult)
  implicit val FullSearchResultsFormats: RootJsonFormat[FullSearchResults] = jsonFormat1(FullSearchResults)

}
