package com.plista.server

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.plista.Models.FullSearchResults
import com.plista.crawler.Crawler
import com.plista.search.SearchService
import com.plista.storage.Storage
import com.plista.{Controller, JsonSupport}
import org.scalatest.{Matchers, WordSpec}

class RoutesTest
  extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with JsonSupport {

  val storage = new Storage()
  val crawler = new Crawler(storage)
  crawler.startCrawling()
  val searchService = new SearchService(storage)
  val controller = new Controller(crawler, searchService)

  "The service" should {

    "Accept urls for crawling" in {
      val request = """{"urls": ["http://0.0.0.0:8081/main"]}"""
      Post("/crawl", HttpEntity(request).withContentType(ContentTypes.`application/json`)) ~> controller.routes ~> check {
        status shouldBe StatusCodes.Accepted
      }
    }

    "Find defined text" in {
      val url = "http://testurl-1.com"
      val content = "page1"
      storage.add(url, content)

      Get("/search?q=page1") ~> controller.routes ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] should (include(url) and include(content))
      }
    }

    "Return empty result for non-existent text" in {
      val url = "http://testurl-1.com"
      val page = "page1"
      storage.add(url, page)

      Get("/search?q=page2") ~> controller.routes ~> check {
        status shouldBe StatusCodes.OK
        responseAs[FullSearchResults].results shouldBe empty
      }
    }
  }
}