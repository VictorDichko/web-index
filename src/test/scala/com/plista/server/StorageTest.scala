package com.plista.server

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.plista.storage.Storage
import org.scalatest.{Matchers, WordSpec}

class StorageTest extends WordSpec with Matchers with ScalatestRouteTest {

  "The storage" should {

    "Add and find correct content" in {
      val storage = new Storage()
      storage.add("testUrl", "test content")
      val searchResult = storage.find("content")
      searchResult.results.map(_.context).mkString should include("content")
    }

    "Add and find correct content with few words" in {
      val storage = new Storage()
      storage.add("testUrl", "test content")
      val searchResult = storage.find("test content")
      searchResult.results.map(_.context).mkString should (include("test") and include("content"))
    }

    "Return empty results if content is missing" in {
      val storage = new Storage()
      storage.add("testUrl", "test content")
      val searchResult = storage.find("other query")
      searchResult.results shouldBe empty
    }

    "Return empty results if content was not added" in {
      val storage = new Storage()
      val searchResult = storage.find("no content")
      searchResult.results shouldBe empty
    }
  }
}