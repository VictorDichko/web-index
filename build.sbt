name := "web-index"

version := "0.1"

scalaVersion := "2.12.7"


lazy val akkaHttpVersion = "10.1.4"
lazy val akkaVersion = "2.5.16"
lazy val scalaTestVersion = "3.0.5"
lazy val scalaScraperVersion = "2.1.0"
lazy val luceneVersion = "1.8.1"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "net.ruippeixotog" %% "scala-scraper" % scalaScraperVersion,
  "com.outr" %% "lucene4s" % luceneVersion,

  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
  "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)

enablePlugins(JavaAppPackaging)